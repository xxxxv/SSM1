package cn.ymx.controller;

import cn.ymx.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {

    @Autowired
    LoginService loginService;

    @RequestMapping("/Login")
    public String Login(String username, String password, Model model) {
        loginService.Login(username,password);
        return "";
    }
}
