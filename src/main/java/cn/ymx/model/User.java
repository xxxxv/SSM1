package cn.ymx.model;

import javax.persistence.Table;
import org.springframework.stereotype.Component;

/**
 * user
 * @author 杨孟新 2019-01-08
 */
@Table(name = "user")

@Component
public class User {
    //LOGO
    private String username;

    //LOGO
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }
}